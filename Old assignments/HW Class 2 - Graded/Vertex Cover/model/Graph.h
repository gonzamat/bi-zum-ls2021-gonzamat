#ifndef VERTEX_COVER_GRAPH_H
#define VERTEX_COVER_GRAPH_H


#define MAX_MATRIX 200

class Graph {

public:
    Graph(int adjancencyMatrix[MAX_MATRIX][MAX_MATRIX], int n);

    int vertexCount();

    bool isVertexCover(int VC[200], int k);

private:
    int m[MAX_MATRIX][MAX_MATRIX];
    int n;
};


#endif //VERTEX_COVER_GRAPH_H
