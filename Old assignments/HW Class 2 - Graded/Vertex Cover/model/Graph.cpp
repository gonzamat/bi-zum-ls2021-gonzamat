#include "Graph.h"

Graph::Graph(int adjancencyMatrix[MAX_MATRIX][MAX_MATRIX], int n) {

    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            this->m[i][j] = adjancencyMatrix[i][j];
        }
    }
    this->n = n;
}

int Graph::vertexCount() {
    return this->n;
}

static bool vertexInList(int v, int VC[200], int n) {
    for(int i = 0; i < n; i++){
        if(VC[i] == v) return true;
    }
    return false;
}

bool Graph::isVertexCover(int VC[200], int k) {

    for(int i = 0; i < this->n; i++){
        for(int j = 0; j < this->n; j++){
            if(this->m[i][j] == 0) continue;

            if(!vertexInList(i, VC, k) && !vertexInList(j, VC, k))
                return false;
        }
    }

    return true;
}
