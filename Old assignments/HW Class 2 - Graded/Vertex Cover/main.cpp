#include <cstdlib>
#include <cstdio>
#include "model/Graph.h"


void findVC(Graph G, int k, int VC[200]);

bool vertexInList(int v, int VC[200], int n);

int findMinVC(Graph G, int VC[200], int tMax);

int main(){

    int k = 2;
    int m[MAX_MATRIX][MAX_MATRIX] = {{0,1,1,0},{1,0,1,1},{1,1,0,1},{0,1,1,0}};
    int n = 4;
    Graph G = Graph(m,n);

    int VertexCover[MAX_MATRIX];
    //findVC(G,k,VertexCover);




    int kMin = findMinVC(G, VertexCover, 30);

    for(int i = 0; i < k; i++){
        printf("%i ", VertexCover[i]);
    }
    printf(" k = %i \n", kMin);

    return 0;
}

int findMinVC(Graph G, int VC[200], int tMax) {
    int k = G.vertexCount();

    for(int i = 0; i < k; i++){
        VC[i] = i;
    }

    for(int t = 0; t < tMax; t++){

        int v, u;
        do{
            v = rand() % G.vertexCount();
        }while(vertexInList(v, VC, k) && k != G.vertexCount());

        int index = rand()%k;
        u = VC[index];

        VC[index] = -1;
        if(G.isVertexCover(VC, k)){
            VC[index] = VC[k-1];
            k = k - 1;
            continue;
        }

        if(k == G.vertexCount()){
            VC[index] = u;
            continue;
        }

        VC[index] = v;
        if(G.isVertexCover(VC, k)){
            continue;
        }

        VC[index] = u;

    }


    return k;
}

void findVC(Graph G, int k, int VC[200]) {

    for(int i = 0; i < k; i++){
        int v;
        do{
            v = rand() % G.vertexCount();
        }while(vertexInList(v, VC, i));
        VC[i] = v;
    }

    while(!G.isVertexCover(VC, k)){
        int v;
        do{
            v = rand() % G.vertexCount();
        }while(vertexInList(v, VC, G.vertexCount()));
        VC[rand()%k] = v;
    }

}

bool vertexInList(int v, int VC[200], int n) {
    for(int i = 0; i < n; i++){
        if(VC[i] == v) return true;
    }
    return false;
}
