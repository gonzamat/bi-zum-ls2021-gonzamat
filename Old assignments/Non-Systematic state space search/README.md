#Non-Systematic State Space Search

**Introduction to Artificial Intelligence**

**Matias Ignacio Gonzalez**

## Task
Implement one method of non-systematic state space search to find the solution to one of these problems:

* N-Queens
* TSP

## Chosen Task

Solve N-Queens using Hill-Climbing, Steep Hill-Climbing and genetic algorithms.

INPUT: N (Number of queens).

OUTPUT: Simple visualization of the operation of each of the algorithms.

## Usage

Select **N**, **MaxSteps** and **algorithm** using program arguments:

> < path > < steps > < algorithm >

**Possible algorithms:**

* hc
* steep
* gen

**Example:**

> 5 300 steep

## Implementation details

Initial state is not completely random: it never has two Queens on the same row.
This is made like this so that it starts close to some maximum.

## Hill Climbing

Each new candidate for this algorithm is chosen this way:
1) Take a random Queen.
2) Move the Queen to a random column.

## Steep Hill Climbing

Each new candidate is chosen the same way as in Hill Climbing, but instead of choosing the first "better-fit", it generates 50 new candidates and chooses the best of them.

## Genetic Algorithm

TBD
