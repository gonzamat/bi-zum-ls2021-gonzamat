#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <cstring>
#include "model/Board.h"


int hillClimb(Board board, int remaining, bool steep){
    printf("\nRemaining: %i\n",remaining);
    printf("Board obj: %i\n",board.objective());
    board.print();

    if(remaining <= 0) return 0;
    if(board.isSolved())
        return remaining;

    Board b = Board(0);
    Board bIter = Board(0);

    do{
        b = Board(board);
        b.newCandidate();

        if(steep){
            for(int i = 0; i < 50; i++){
                bIter = Board(board);
                bIter.newCandidate();
                if(b.objective() < bIter.objective())
                    b = bIter;
            }
        }

    }while(b.objective() < board.objective());

    return hillClimb(b, remaining-1, steep);
}

int main(int argc, char* argv[]){
    srand(time(NULL));

    int n = atoi(argv[1]);
    int tMax = atoi(argv[2]);

    Board board = Board(n);

    int remaining = hillClimb(board, tMax, strcmp(argv[3],"steep") == 0);

    if(remaining > 0){
        printf("Solved in %i steps.\n", tMax - remaining);
    }else{
        printf("Unfeasible solution\n");
    }

    return 0;
}