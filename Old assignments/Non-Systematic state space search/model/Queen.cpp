#include "Queen.h"

void Queen::setPos(int x, int y) {
    this->x = x;
    this->y = y;
}

bool Queen::isInX(int x) {
    return x == this->x;
}

bool Queen::isInY(int y) {
    return y == this->y;
}

bool Queen::attacks(Queen queen) {

    bool attacksX = queen.isInX(this->x);
    bool attacksY = queen.isInY(this->y);
    bool attacksNegDiag = queen.negativeDiagFactor() == this->negativeDiagFactor();
    bool attacksPosDiag = queen.positiveDiagFactor() == this->positiveDiagFactor();

    return attacksX || attacksY || attacksNegDiag || attacksPosDiag;
}


int Queen::negativeDiagFactor() {
    return this->x - this->y;
}

int Queen::positiveDiagFactor() {
    return this->x + this->y;
}


