#ifndef NON_SYSTEMATIC_STATE_SPACE_SEARCH_QUEEN_H
#define NON_SYSTEMATIC_STATE_SPACE_SEARCH_QUEEN_H


class Queen {

public:
    void setPos(int x, int y);

    bool isInX(int x);

    bool attacks(Queen queen);

private:

    int x, y;

    bool isInY(int y);

    int negativeDiagFactor();

    int positiveDiagFactor();
};


#endif //NON_SYSTEMATIC_STATE_SPACE_SEARCH_QUEEN_H
