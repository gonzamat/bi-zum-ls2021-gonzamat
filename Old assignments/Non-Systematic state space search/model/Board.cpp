#include <cstdlib>
#include <cstdio>
#include "Board.h"

Board::Board(int n) {
    this->n = n;
    for(int i = 0; i < n; i++){
        this->queens[i].setPos(rand() % n, i);
    }


}

void Board::print() {

    for(int i = 0; i < this->n; i++){
        for(int j = 0; j < this->n; j++){
            printf("[%c]", this->queens[i].isInX(j) ? 'Q' : ' ');
        }
        printf("\n");
    }

}

bool Board::isSolved() {

    for(int i = 0; i < this->n; i++){
        for(int j = i+1; j < this->n; j++){
            if(this->queens[i].attacks(this->queens[j]))
                return false;
        }
    }

    return true;
}

void Board::newCandidate() {
    int queenIndex = rand() % n;
    this->queens[queenIndex].setPos(rand() % n, queenIndex);
}

int Board::objective() {
    int objective = 0;
    for(int i = 0; i < this->n; i++){
        for(int j = 0; j < this->n; j++){
            if(i == j) continue;
            if(this->queens[i].attacks(this->queens[j]))
                objective--;
        }
    }

    return objective;
}
