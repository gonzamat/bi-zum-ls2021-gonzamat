#ifndef NON_SYSTEMATIC_STATE_SPACE_SEARCH_BOARD_H
#define NON_SYSTEMATIC_STATE_SPACE_SEARCH_BOARD_H


#include "Queen.h"

class Board {

public:
    Board(int n);

    void print();

    bool isSolved();

    void newCandidate();

    int objective();

private:

    Queen queens[100];

    int n;
};


#endif //NON_SYSTEMATIC_STATE_SPACE_SEARCH_BOARD_H
