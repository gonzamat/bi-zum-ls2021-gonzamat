cmake_minimum_required(VERSION 3.20)
project(Non-systematic_state_space_search)

set(CMAKE_CXX_STANDARD 14)

add_executable(Non-systematic_state_space_search main.cpp model/Board.cpp model/Board.h model/Queen.cpp model/Queen.h)
