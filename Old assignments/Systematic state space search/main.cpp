#include <cstdio>
#include <queue>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include "model/Maze.h"


int abs(int j) {
    if (j < 0) return -j;
    return j;
}


bool depthSearch(Maze *maze, Coordinate dir, Steps *steps) {
    if(maze->won()){
        return true;
    }

    if(!maze->explore(dir)){
        return false;
    }

    for(int i = -1; i <= 1; i++){
        for(int j = -1; j <= 1; j++){
            if(abs(i) + abs(j) != 1) continue;
            steps->steps++;
            if(depthSearch(maze, Coordinate(i, j), steps)){
                steps->length++;
                maze->backtrack(dir);
                maze->markPath();
                return true;
            }
        }
    }

    maze->backtrack(dir);
    return false;
}

Steps widthSearch(Maze *maze){
    Steps steps;
    steps.steps = 0;
    steps.length = 0;

    std::queue<Coordinate> q = std::queue<Coordinate>();
    maze->initQueue(&q);

    while(!q.empty()){

        Coordinate pos = q.front();
        q.pop();
        steps.steps++;
        if(maze->exploreCoordinate(pos)){
            maze->reconstructPath(pos, &steps);
            return steps;
        }

        for(int i = -1; i <= 1; i++){
            for(int j = -1; j <= 1; j++){
                if(abs(i) + abs(j) != 1) continue;
                Coordinate dir = Coordinate(i,j);
                dir.move(pos);
                if(maze->explorable(dir)){
                    dir.setPrev(pos);
                    q.push(dir);
                }
            }
        }

    }

    steps.length = -1;
    return steps;
}


Steps randomSearch(Maze *maze){

    srand(time(NULL));
    Coordinate openNodes[MAX_GRID*MAX_GRID];
    int nodeCount = 0;
    maze->initOpenNodes(openNodes, &nodeCount);
    Steps steps;
    steps.steps = 0;
    steps.length = 0;

    while(nodeCount > 0){
        int openIndex = rand()%nodeCount;
        Coordinate pos = openNodes[openIndex];
        openNodes[openIndex] = openNodes[--nodeCount];
        steps.steps++;
        if(maze->exploreCoordinate(pos)){
            maze->reconstructPath(pos, &steps);
            return steps;
        }

        for(int i = -1; i <= 1; i++){
            for(int j = -1; j <= 1; j++){
                if(abs(i) + abs(j) != 1) continue;
                Coordinate dir = Coordinate(i,j);
                dir.move(pos);
                if(maze->explorable(dir)){
                    dir.setPrev(pos);
                    openNodes[nodeCount] = dir;
                    nodeCount++;
                }
            }
        }

    }

    steps.length = -1;
    return steps;
}


Steps greedySearch(Maze *maze){

    Coordinate openNodes[MAX_GRID*MAX_GRID];
    int openCount = 0;
    maze->initOpenNodes(openNodes, &openCount);
    Steps steps;
    steps.steps = 0;
    steps.length = 0;


    while(openCount > 0){

        Coordinate pos = maze->greedyNode(openNodes, &openCount);
        steps.steps++;
        if(maze->exploreCoordinate(pos)){
            maze->reconstructPath(pos, &steps);
            return steps;
        }

        for(int i = -1; i <= 1; i++){
            for(int j = -1; j <= 1; j++){
                if(abs(i) + abs(j) != 1) continue;
                Coordinate dir = Coordinate(i,j);
                dir.move(pos);
                if(maze->explorable(dir)){
                    dir.setPrev(pos);
                    openNodes[openCount++] = dir;
                }
            }
        }
    }

    steps.length = -1;
    return steps;
}

Steps AStarSearch(Maze *maze){

    Coordinate openNodes[MAX_GRID*MAX_GRID];
    int openCount = 0;
    maze->initOpenNodes(openNodes, &openCount);

    Steps steps;
    steps.steps = 0;
    steps.length = 0;
    int distances[MAX_GRID][MAX_GRID];
    for(auto & distance : distances){
        for(int & j : distance){
            j = 999999;
        }
    }

    while(openCount > 0){

        Coordinate pos = maze->getAStarNode(openNodes, &openCount, distances);
        steps.steps++;
        if(maze->exploreCoordinate(pos)){
            maze->reconstructPath(pos, &steps);
            return steps;
        }

        for(int i = -1; i <= 1; i++){
            for(int j = -1; j <= 1; j++){
                if(abs(i) + abs(j) != 1) continue;
                Coordinate dir = Coordinate(i,j);
                dir.move(pos);
                if(maze->explorable(dir)){
                    dir.setPrev(pos);
                    distances[dir.getX()][dir.getY()] = distances[pos.getX()][pos.getY()] + 1;
                    openNodes[openCount++] = dir;
                }
            }
        }
    }

    steps.length = -1;
    return steps;
}



int main(int argc, char* argv[]) {
    if(argc != 3) return -1;

    FILE* mazeFile = fopen(argv[1], "r");
    if(!mazeFile){
        perror("fopen");
        return -1;
    }
    Maze maze = Maze(mazeFile);
    fclose(mazeFile);
    Steps steps;
    steps.steps = 0;
    steps.length = 0;

    if(strcmp(argv[2], "dfs") == 0)
        depthSearch(&maze, Coordinate(), &steps);
    else if(strcmp(argv[2], "bfs") == 0)
        steps = widthSearch(&maze);
    else if(strcmp(argv[2], "rs") == 0)
        steps = randomSearch(&maze);
    else if(strcmp(argv[2], "gs") == 0)
        steps = greedySearch(&maze);
    else if(strcmp(argv[2], "as") == 0)
        steps = AStarSearch(&maze);

    maze.printMaze();

    printf("Steps: %i Lenght: %i\n", steps.steps, steps.length);

    return 0;
}

