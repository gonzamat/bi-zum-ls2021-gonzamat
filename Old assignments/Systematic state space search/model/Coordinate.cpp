#include "Coordinate.h"

#define NULL nullptr


Coordinate::Coordinate(int x, int y) {
    this->x = x;
    this->y = y;
    this->prev = NULL;
}

void Coordinate::move(Coordinate direction) {
    this->x += direction.getX();
    this->y += direction.getY();
}

int Coordinate::getX() {
    return this->x;
}

int Coordinate::getY() {
    return this->y;
}

Coordinate::Coordinate() {
    this->x = 0;
    this->y = 0;
}

void Coordinate::markVisited(char grid[300][300]) {
    grid[this->x][this->y] = 'V';
}

bool Coordinate::validCoordinate(char grid[300][300], Coordinate limit) {
    return (grid[this->x][this->y] == ' ') && (this->insideLimits(limit));
}

bool Coordinate::insideLimits(Coordinate limit) {
    return this->x <= limit.x && this->y <= limit.y;
}

bool Coordinate::is(Coordinate coordinate) {
    return this->x == coordinate.x && this->y == coordinate.y;
}

void Coordinate::markPath(char grid[300][300]) {
    grid[this->x][this->y] = '@';
}

void Coordinate::reverse() {
    this->x *= -1;
    this->y *= -1;
}


Coordinate::~Coordinate() {
    //delete(this->prev);
}

void Coordinate::reconstructPath(char grid[300][300], int *length) {
    this->markPath(grid);
    (*length)++;
    if(!this->prev) return;
    this->prev->reconstructPath(grid, length);
}

void Coordinate::setPrev(Coordinate coordinate) {
    this->prev = new Coordinate(coordinate.getX(), coordinate.getY(), coordinate.getPrev());
}

static int abs(int j) {
    if (j < 0) return -j;
    return j;
}

int Coordinate::manhattanDistance(Coordinate coordinate) {
    return abs(this->x - coordinate.getX()) + abs(this->y - coordinate.getY());
}

Coordinate::Coordinate(int x, int y, Coordinate *prev) {
    this->x = x;
    this->y = y;
    this->prev = prev;
}

Coordinate *Coordinate::getPrev() {
    return this->prev;
}


