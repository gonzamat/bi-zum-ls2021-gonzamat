#ifndef SYSTEMATIC_STATE_SPACE_SEARCH_MAZE_H
#define SYSTEMATIC_STATE_SPACE_SEARCH_MAZE_H

#include <queue>
#include "Coordinate.h"

#define MAX_GRID 300

typedef struct Steps {
    int steps;
    int length;
} Steps;

class Maze {

public:
    Maze(FILE* mazeFile);

    ~Maze();

    void printMaze();

    bool explore(Coordinate direction);

    bool won();

    void markPath();

    void backtrack(Coordinate coordinate);

    bool explorable(Coordinate coordinate);

    bool exploreCoordinate(Coordinate coordinate);

    void initQueue(std::queue<Coordinate> *q);

    void reconstructPath(Coordinate coordinate, Steps *steps);

    void initOpenNodes(Coordinate openNodes[90000], int *openNodesCount);

    Coordinate getAStarNode(Coordinate openNodes[90000], int *openCount, int distances[300][300]);

    Coordinate greedyNode(Coordinate openNodes[90000], int *openCount);

private:
    char grid[MAX_GRID][MAX_GRID];

    Coordinate pos, start, end, limit;

};


#endif //SYSTEMATIC_STATE_SPACE_SEARCH_MAZE_H
