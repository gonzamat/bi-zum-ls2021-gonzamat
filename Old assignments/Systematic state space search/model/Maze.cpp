#include <cstdio>
#include <cstring>
#include "Maze.h"

Maze::Maze(FILE* mazeFile) {

    char line[MAX_GRID+1];
    int lineNumber = 0;

    while(fgets(line, MAX_GRID, mazeFile) && line[0] != 's'){
        line[strlen(line) - 1] = '\0';
        strcpy(this->grid[lineNumber], line);
        lineNumber++;
    }
    if(lineNumber < 1){
        fclose(mazeFile);
        return;
    }

    this->limit = Coordinate(lineNumber, (int) strlen(this->grid[0]));

    int x, y;
    sscanf(line, "start %i, %i\n", &y, &x);
    this->start = Coordinate(x, y);
    this->pos = Coordinate(this->start);
    fgets(line, MAX_GRID, mazeFile);
    sscanf(line, "end %i, %i\n", &y, &x);
    this->end = Coordinate(x, y);


}

void Maze::printMaze() {

    for(int i = 0; i < this->limit.getX(); i++){
        for(int j = 0; j < this->limit.getY(); j++){
            if(this->end.is(Coordinate(i,j))) {
                printf("E ");
            }else if(this->start.is(Coordinate(i,j))){
                printf("S ");
            }else{
                printf("%c ", this->grid[i][j]);
            }

        }
        printf("\n");
    }
}

bool Maze::explore(Coordinate direction) {
    if(!direction.is(Coordinate()))
        this->pos.markVisited(this->grid);

    Coordinate newCoord = Coordinate(this->pos);
    newCoord.move(direction);

    if(!newCoord.validCoordinate(this->grid, this->limit))
        return false;

    this->pos.move(direction);
    return true;
}

bool Maze::won() {
    return this->pos.is(this->end);
}

void Maze::markPath() {
    this->pos.markPath(this->grid);
}

void Maze::backtrack(Coordinate coordinate) {
    coordinate.reverse();
    this->pos.move(coordinate);
}

bool Maze::explorable(Coordinate coordinate) {
    return coordinate.validCoordinate(this->grid, this->limit);
}

bool Maze::exploreCoordinate(Coordinate c) {
    if(c.is(this->end))
        return true;

    c.markVisited(this->grid);
    return false;
}

void Maze::initQueue(std::queue<Coordinate> *q) {
    q->push(this->start);
}

void Maze::reconstructPath(Coordinate coordinate, Steps *steps) {
    coordinate.reconstructPath(this->grid, &(steps->length));
}

void Maze::initOpenNodes(Coordinate openNodes[MAX_GRID*MAX_GRID], int *openNodesCount) {
    openNodes[0] = Coordinate(this->pos);
    *openNodesCount = 1;
}


Coordinate Maze::getAStarNode(Coordinate openNodes[300*300], int *openCount, int distances[300][300]) {

    int min = 0;


    for(int i = 0; i < *openCount; i++){
        int newDistance =
                openNodes[i].manhattanDistance(this->end) + distances[openNodes[i].getX()][openNodes[i].getY()];
        int minDistance =
                openNodes[min].manhattanDistance(this->end) + distances[openNodes[min].getX()][openNodes[min].getY()];
        if(newDistance < minDistance)
            min = i;
    }
    Coordinate AStarNode = openNodes[min];
    openNodes[min] = openNodes[--(*openCount)];
    return AStarNode;
}

Coordinate Maze::greedyNode(Coordinate openNodes[MAX_GRID*MAX_GRID], int *openCount) {
    int min = 0;

    for(int i = 0; i < *openCount; i++){
        int newDistance = openNodes[i].manhattanDistance(this->end);
        int minDistance = openNodes[min].manhattanDistance(this->end);
        if(newDistance < minDistance)
            min = i;
    }

    Coordinate greedyNode = openNodes[min];
    openNodes[min] = openNodes[--(*openCount)];
    return greedyNode;
}

Maze::~Maze() = default;
