#ifndef SYSTEMATIC_STATE_SPACE_SEARCH_COORDINATE_H
#define SYSTEMATIC_STATE_SPACE_SEARCH_COORDINATE_H

class Coordinate;

class Coordinate {

public:
    Coordinate(int x, int y);

    Coordinate(int x, int y, Coordinate* prev);

    Coordinate();

    ~Coordinate();

    void move(Coordinate direction);

    int getX();

    int getY();

    bool validCoordinate(char grid[300][300], Coordinate limit);

    bool insideLimits(Coordinate limit);

    void markVisited(char grid[300][300]);

    bool is(Coordinate coordinate);

    void markPath(char grid[300][300]);

    void reverse();

    void reconstructPath(char grid[300][300], int *length);

    void setPrev(Coordinate coordinate);

    int manhattanDistance(Coordinate coordinate);

private:

    int x{}, y{};

    Coordinate *prev;

    Coordinate *getPrev();
};


#endif //SYSTEMATIC_STATE_SPACE_SEARCH_COORDINATE_H
