#Systematic State Space Search

**Introduction to Artificial Intelligence**

**Matias Ignacio Gonzalez**

## Task
Implement five methods of systematic state space search to find a path in a maze.

* Random Search
* Depth Search (DFS)
* Width Search (BFS)
* Greedy Search
* A *

INPUT: Maze for testing algorithms. Mazes are available in the form of text files.

OUTPUT: Simple visualization of the operation of each of the algorithms.

## Usage

Select **maze** and **algorithm** using program arguments:

> < path > < algorithm >

**Possible algorithms:**

* dfs
* bfs
* rs
* gs
* as

**Example:**

> dataset/26.txt gs

## Heuristic

Chosen Heuristic for **Greedy Search** and **A\* Search**:

h(s) = manhattan_distance(s, end) <= h*(s)

## Results

**Notation:**

* X: Wall
* S: Start
* E: End
* V: Visited
* @: Path

### Maze 36

#### Random Search
```
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
X                           V V V V V V V V V V V V V V V V V V V V V V V V @ @ @ @ @ @ @ @ V V V V X V V V X 
X     X X   X X X X       X V V X X X X X V X X X X X V V X V V X X X X @ @ @ X X X X V X @ X V V V X V X V X 
X   X                       V V @ @ @ @ @ @ X V @ @ X V V V X V V @ @ @ @ V X V V V V V V S X V X V X V X V X 
X       X X   X     X X X X X X @ X X X X @ X @ @ @ X X V V X X X @ V X V V V V X X V X X X V V X V X X X V X 
X               X         E @ @ @ V V V V @ @ @ X @ @ @ @ @ @ @ @ @ V V V V X V V V V V V V V V X V V V V V X 
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
Steps: 181 Lenght: 47

X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
X                             V V V V V V V V V V V V V V V V V V V V V V V @ @ @ @ @ @ @ @ V V V V X V V V X 
X     X X   X X X X       X     X X X X X V X X X X X V V X V V X X X X @ @ @ X X X X V X @ X V V V X V X V X 
X   X                                   V V X V V V X V V V X V V @ @ @ @ V X V V V V V V S X V X V X   X V X 
X       X X   X     X X X X X X V X X X X V X @ @ @ X X V V X X X @ V X V V V V X X V X X X V V X V X X X V X 
X               X         E @ @ @ @ @ @ @ @ @ @ X @ @ @ @ @ @ @ @ @ V V V V X V V V V V V V V V X V V V V V X 
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
Steps: 165 Lenght: 41
```

#### Depth Search (DFS)
```
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
X @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @         X       X 
X @ V X X V X X X X @ @ @ X @ @ X X X X X V X X X X X     X     X X X X       X X X X   X @ X       X   X   X 
X @ X @ @ @ @ @ @ @ @ V @ @ @ @ @ @ @ @ @ @ X       X       X               X             S X   X   X   X   X 
X @ @ @ X X   X     X X X X X X V X X X X @ X       X X     X X X     X         X X   X X X     X   X X X   X 
X               X         E @ @ @ @ @ @ @ @     X                           X                   X           X 
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
Steps: 218 Lenght: 85
```

#### Width Search (BFS)
```
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
X                     V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V V X V V V X 
X     X X   X X X X     V X V V X X X X X V X X X X X V V X V V X X X X V V V X X X X V X V X V V V X V X V X 
X   X                     V V V V V V V V V X V V V X V V V X V V V V V V V X @ @ @ @ @ @ S X V X V X V X V X 
X       X X   X     X X X X X X V X X X X V X @ @ @ X X V V X X X V V X @ @ @ @ X X V X X X V V X V X X X V X 
X               X         E @ @ @ @ @ @ @ @ @ @ X @ @ @ @ @ @ @ @ @ @ @ @ V X V V V V V V V V V X V V V V V X 
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
Steps: 351 Lenght: 37
```

#### Greedy Search
```
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
X                                                                                                   X       X 
X     X X   X X X X       X     X X X X X   X X X X X     X     X X X X       X X X X   X   X       X   X   X 
X   X                                       X       X       X               X @ @ @ @ @ @ S X   X   X   X   X 
X       X X   X     X X X X X X   X X X X   X @ @ @ X X     X X X     X @ @ @ @ X X   X X X     X   X X X   X 
X               X         E @ @ @ @ @ @ @ @ @ @ X @ @ @ @ @ @ @ @ @ @ @ @   X                   X           X 
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
Steps: 37 Lenght: 37
```
#### A *
```
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
X                                                                                                   X       X 
X     X X   X X X X       X     X X X X X   X X X X X     X     X X X X       X X X X V X V X       X   X   X 
X   X                                       X       X       X       V V V V X @ @ @ @ @ @ S X   X   X   X   X 
X       X X   X     X X X X X X   X X X X   X @ @ @ X X     X X X V V X V @ @ @ X X V X X X     X   X X X   X 
X               X         E @ @ @ @ @ @ @ @ @ @ X @ @ @ @ @ @ @ @ @ @ @ @ @ X V V V V V         X           X 
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
Steps: 70 Lenght: 37
```

### Analysis
On this test, Greedy Search got the shortest path in the minimum amount of steps.
This does **NOT** mean that Greedy Search is the most efficient algorithm to find the shortest path.
If we take Maze 72:

#### Greedy Search
```
X   X X X X X     X X X X   X     X X X X X             X X     X X X X X     X         X X X   X X X X X X X 
X           X   X       X   X       X           X                               X   X   X           X @ @ S X 
X X   X                 X       X X X     X     X   X X X X X   X     X X X X     X     X X X X X   X @ X X X 
X           X           X   X       X                               X       X           X           X @     X 
X   X   X X X   X   X X X       X   X   X   X   X X X   X   X X X   X X X   X X X   X       X   X   X @ X   X 
X   X       X   X       X       X   X   X   X @ @ @     X @ @ @ @ @ @ @ @ @ @ @ @                   X @ @ @ X 
X       X X X   X       X X X X     X       X @ X @ X X X @ X X     X X   X X X @ @       X X   X   X X X @ X 
X       X     @ @ @ @ @ @ @ @ @ @ @ @ @     X @ X @ @ @ X @ @ @                 X @ @ @ @           X @ @ @ X 
X X X       X @ X X X X     X   X X X @ X   X @   X X @ X X X @ X   X     X X     X X X @ @ X X     X @ X   X 
X             E                 X   X @ @ @ @ @     X @ @ @ X @ X           X           X @ @ @ @ @ X @ V   X 
X     X X   X X X X X   X X X       X X X X X   X   X V X @ X @ X     X     X X X   X X X X X   X @ @ @ X   X 
X                               X                   X V   @ @ @ X       X   X   X               X       X   X 
X X     X X     X   X   X   X   X   X     X X   X     X   X   X   X     X   X   X X X X X X   X       X     X 
X                       X       X           X       X   X                       X               X           X 
X     X X X X X   X X X X X X   X   X X X         X     X   X X X   X X X X     X   X X     X     X X X X   X 
X       X                       X                               X           X   X       X   X               X 
X X X   X X X   X X   X X   X   X X X X X   X   X   X X   X     X   X X X   X X X       X       X   X X     X 
X                           X                                           X                                   X 
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
Steps: 92 Lenght: 89
```
#### A *
```
X   X X X X X     X X X X   X     X X X X X             X X     X X X X X     X         X X X   X X X X X X X 
X           X   X       X   X       X           X                               X   X   X           X @ @ S X 
X X   X                 X       X X X     X     X   X X X X X   X     X X X X     X     X X X X X   X @ X X X 
X           X           X   X       X V V @ @ @ @ @ @ @ @ @ V V V V X V V V X V V V V V X V V V     X @ V V X 
X   X   X X X   X   X X X       X   X V X @ X V X X X V X @ X X X V X X X V X X X V X V V V X V X   X @ X V X 
X   X       X   X       X       X   X V X @ X V V V V V X @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ V V V V V V X @ @ @ X 
X       X X X   X       X X X X     X V   @ X V X V X X X V X X V V X X V X X X V V V @ @ X X V X V X X X @ X 
X       X     V V V V V @ @ @ @ @ @ @ @ @ @ X V X V     X V V V V V V V V V V V X V V V @ V V V V V X V V @ X 
X X X       X   X X X X @ V X V X X X V X V X V   X X   X X X V X V X V V X X V V X X X @ @ X X V V X V X @ X 
X             E @ @ @ @ @ V   V X   X V V V V V     X V V V X V X V V V V V X V V V V V X @ @ @ @ @ X @ @ @ X 
X     X X   X X X X X   X X X       X X X X X   X   X V X V X V X V V X V V X X X V X X X X X V X @ @ @ X V X 
X                               X                   X V V V V V X V V   X   X   X V V V V V V V X V V V X V X 
X X     X X     X   X   X   X   X   X     X X   X     X   X   X   X     X   X   X X X X X X V X V V V X V V X 
X                       X       X           X       X   X                       X V V V V V V V X V V V V V X 
X     X X X X X   X X X X X X   X   X X X         X     X   X X X   X X X X     X V X X V V X V V X X X X V X 
X       X                       X                               X           X   X V     X V X V V V V V V V X 
X X X   X X X   X X   X X   X   X X X X X   X   X   X X   X     X   X X X   X X X       X       X V X X V V X 
X                           X                                           X                                   X 
X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X 
Steps: 413 Lenght: 73
```

Greedy Search found a path much faster, but it is not the optimal. A* found the shortest path but with more steps.