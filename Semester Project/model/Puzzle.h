#ifndef SUDOKU_PUZZLE_H
#define SUDOKU_PUZZLE_H

#define GRID_LEN 81
#define LINE_LEN 9

class Puzzle {
public:
    Puzzle();

    explicit Puzzle(const int original[GRID_LEN], const int puzzle[GRID_LEN]);

    void fillNextCell(int n);

    bool isValid();

    bool isSolved();

    int *getOriginal();

    int *getPuzzle();

    void print();

    void fillSmartCell(int n, int x, int y);

    void getSmartCell(int* x, int* y);

    void newCandidate();

    int objective();

    void fill();

private:
    int original[GRID_LEN];
    int puzzle[GRID_LEN];

    bool validRows();

    bool validColumns();

    bool validSquares();

    bool validSquare(int x, int y);

    int getPossibilities(int x, int y);

    void shuffle(int row);
};


#endif //SUDOKU_PUZZLE_H
