#include <cstdio>
#include <cstdlib>
#include "Puzzle.h"

Puzzle::Puzzle(const int original[GRID_LEN], const int puzzle[GRID_LEN]) {
    for(int i = 0; i < GRID_LEN; i++){
        this->original[i] = original[i];
        this->puzzle[i] = puzzle[i];
    }
}

void Puzzle::fillNextCell(int n) {
    for(int & i : this->puzzle){
        if(i == 0){
            i = n;
            return;
        }
    }
}

bool Puzzle::isValid() {
    return this->validRows() && this->validColumns() && this->validSquares();
}

bool Puzzle::validRows() {
    for(int i = 0; i < LINE_LEN; i++){
        for(int j = 0; j < LINE_LEN-1; j++){
            if(this->puzzle[i*LINE_LEN + j] == 0) continue;
            for(int k = j+1; k < LINE_LEN; k++){
                if(this->puzzle[i*LINE_LEN + j] == this->puzzle[i*LINE_LEN + k])
                    return false;
            }
        }
    }
    return true;
}

bool Puzzle::validColumns() {
    for(int j = 0; j < LINE_LEN; j++){
        for(int i = 0; i < LINE_LEN-1; i++){
            if(this->puzzle[i*LINE_LEN + j] == 0) continue;
            for(int k = i+1; k < LINE_LEN; k++){
                if(this->puzzle[i*LINE_LEN + j] == this->puzzle[k*LINE_LEN + j])
                    return false;
            }
        }
    }
    return true;
}

bool Puzzle::validSquares() {
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            if(!this->validSquare(i,j))
                return false;
        }
    }
    return true;
}

bool Puzzle::validSquare(int x, int y) {
    int square[LINE_LEN];
    int count = 0;
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            if(this->puzzle[(i +x*3)*LINE_LEN + y*3 + j] == 0) continue;
            square[count++] = this->puzzle[(i +x*3)*LINE_LEN + y*3 + j];
        }
    }
    for(int i = 0; i < count-1; i++){
        for(int j = i+1; j < count; j++){
            if(square[i] == square[j])
                return false;
        }
    }

    return true;
}

bool Puzzle::isSolved() {
    for(int & i : this->puzzle){
        if(i == 0 ) return false;
    }
    return this->isValid();
}

int *Puzzle::getOriginal() {
    return this->original;
}

int *Puzzle::getPuzzle() {
    return this->puzzle;
}

void Puzzle::print() {
    for(int i = 0; i < LINE_LEN; i++){
        for(int j = 0; j < LINE_LEN; j++){
            printf("%i ", this->puzzle[i*LINE_LEN+j]);
        }
        printf("\n");
    }
}

Puzzle::Puzzle() {

}

void Puzzle::fillSmartCell(int n, int x, int y) {
    if(n == 0) return;
    this->puzzle[x*LINE_LEN + y] = n;
}

int Puzzle::getPossibilities(int x, int y) {
    int rows = 0;
    for(int i = 0; i < LINE_LEN; i++){
        if(this->puzzle[x*LINE_LEN + i] == 0)
            rows++;
    }

    int cols = 0;
    for(int i = 0; i < LINE_LEN; i++){
        if(this->puzzle[i*LINE_LEN + y] == 0)
            cols++;
    }

    int squares = 0;
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            if(this->puzzle[(i +(x%3)*3)*LINE_LEN + (y%3)*3 + j] == 0)
                squares++;
        }
    }
    int min = (rows<cols)?rows:cols;

    return (min<squares)?min:squares;
}

void Puzzle::getSmartCell(int *x, int* y) {
    int best = 999999;
    int iters = 0;
    for(int i = 0; i < LINE_LEN; i++){
        for(int j = 0; j < LINE_LEN; j++){
            if(this->puzzle[i*LINE_LEN + j] != 0) continue;
            iters++;
            int possibilities = this->getPossibilities(i, j);
            if(possibilities < best){
                best = possibilities;
                *x = i;
                *y = j;
            }
            if(possibilities < 5 || iters > 5){
                return;
            }
        }
    }
}

void Puzzle::newCandidate() {
    this->shuffle(rand() % LINE_LEN);
}

void remove(int element, int row[LINE_LEN], int* count){
    for(int i = 0; i < *count; i++){
        if(row[i] == element) {
            (*count)--;
            row[i] = row[*count];
            return;
        }
    }
}

void Puzzle::shuffle(int row) {
    int candidates[LINE_LEN] = {1,2,3,4,5,6,7,8,9};
    int count = 9;
    for(int i = 0; i < LINE_LEN; i++){
        remove(this->original[row*LINE_LEN + i], candidates, &count);
    }
    for(int i = 0; i < LINE_LEN; i++){
        this->puzzle[row*LINE_LEN + i] = this->original[row*LINE_LEN + i];
    }
    for(int i = 0; i < count; i++){
        int col;
        do {
            col = rand()%LINE_LEN;
        }while(this->puzzle[row*LINE_LEN + col] != 0);
        this->puzzle[row*LINE_LEN + col] = candidates[i];
    }
}

int Puzzle::objective() {
    int rows = LINE_LEN;
    for(int i = 0; i < LINE_LEN; i++){
        for(int j = 0; j < LINE_LEN-1; j++){
            for(int k = j+1; k < LINE_LEN; k++){
                if(this->puzzle[i*LINE_LEN + j] == this->puzzle[i*LINE_LEN + k])
                    rows--;
            }
        }
    }

    int cols = LINE_LEN;
    for(int j = 0; j < LINE_LEN; j++){
        for(int i = 0; i < LINE_LEN-1; i++){
            for(int k = i+1; k < LINE_LEN; k++){
                if(this->puzzle[i*LINE_LEN + j] == this->puzzle[k*LINE_LEN + j])
                    cols--;
            }
        }
    }

    int squares = LINE_LEN;
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            if(!this->validSquare(i,j))
                squares--;
        }
    }

    return rows+cols+squares;
}

void Puzzle::fill() {
    for(int i = 0; i < LINE_LEN; i++)
        this->shuffle(i);
}
