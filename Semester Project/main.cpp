#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include "model/Puzzle.h"
#define REMAINING 1000

bool solvePuzzleDFS(Puzzle puzzle, int n);

Puzzle *loadPuzzles(int *count, char* path);

bool solvePuzzleSmartDFS(Puzzle puzzle, int n, int x, int y);

bool solvePuzzleHillClimbing(Puzzle puzzle, int remaining, bool steep);

int main() {

    Puzzle* puzzles;
    int puzzleCount;
    puzzles = loadPuzzles(&puzzleCount, "./puzzles/sudoku.txt");

    clock_t begin, end;
    double time_spent;

    printf("DFS:\n");
    begin = clock();
    int dfsCount = 0;
    for(int i = 0; i < puzzleCount; i++){
        if(solvePuzzleDFS(puzzles[i], 0))
            dfsCount++;
    }
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Solved %i puzzles out of %i in %f seconds\n", dfsCount, puzzleCount, time_spent);

    printf("Smart DFS:\n");
    begin = clock();
    int smartDfsCount = 0;
    for(int i = 0; i < puzzleCount; i++){
        if(solvePuzzleSmartDFS(puzzles[i], 0, 0, 0))
            smartDfsCount++;
    }
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Solved %i puzzles out of %i in %f seconds\n", smartDfsCount, puzzleCount, time_spent);

    free(puzzles);
    puzzles = loadPuzzles(&puzzleCount, "./puzzles/sudoku_short.txt");

    printf("Hill Climbing:\n");
    begin = clock();
    int hillClimbingCount = 0;
    for(int i = 0; i < puzzleCount; i++){

        if(solvePuzzleHillClimbing(puzzles[i], REMAINING, true))
            hillClimbingCount++;
    }
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Solved %i puzzles out of %i in %f seconds\n", hillClimbingCount, puzzleCount, time_spent);


    return 0;
}

bool solvePuzzleHillClimbing(Puzzle puzzle, int remaining, bool steep) {
    if(remaining == REMAINING) puzzle.fill();

    if(remaining <= 0) return false;

    if(puzzle.isSolved())
        return true;

    Puzzle p = Puzzle();
    Puzzle pIter = Puzzle();

    do{
        p = Puzzle(puzzle);
        p.newCandidate();

        if(steep){
            for(int i = 0; i < 200; i++){
                pIter = Puzzle(puzzle);
                pIter.newCandidate();
                if(p.objective() < pIter.objective())
                    p = pIter;
            }
        }

    }while(p.objective() < puzzle.objective());

    return solvePuzzleHillClimbing(p, remaining-1, steep);

}

bool solvePuzzleSmartDFS(Puzzle puzzle, int n, int x, int y) {
    puzzle.fillSmartCell(n, x, y);

    if(!puzzle.isValid())
        return false;

    if(puzzle.isSolved())
        return true;

    int x_s, y_s;
    puzzle.getSmartCell(&x_s, &y_s);
    for(int i = 1; i < 10; i++){
        Puzzle newPuzzle = Puzzle(puzzle.getOriginal(), puzzle.getPuzzle());
        if(solvePuzzleSmartDFS(newPuzzle, i, x_s, y_s))
            return true;
    }

    return false;
}

Puzzle *loadPuzzles(int *count, char* path) {
    FILE* puzzleFile = fopen(path, "r");

    Puzzle* puzzles = nullptr;
    *count = 0;
    char line[100];
    while(fgets(line, 100, puzzleFile)){
        if (line[strlen(line) - 1] == '\n')
            line[strlen(line) - 1] = '\0';

        int puzzle_line[GRID_LEN];
        for(int i = 0; i < GRID_LEN; i++){
            puzzle_line[i] = (int) (line[i] - '0');
        }
        puzzles = (Puzzle*) realloc((void*)puzzles, (++(*count))*sizeof(Puzzle));
        puzzles[(*count)-1] = Puzzle(puzzle_line, puzzle_line);
    }
    return puzzles;
}

bool solvePuzzleDFS(Puzzle puzzle, int n) {
    puzzle.fillNextCell(n);

    if(!puzzle.isValid())
        return false;

    if(puzzle.isSolved()){
        return true;
    }

    for(int i = 1; i < 10; i++){
        Puzzle newPuzzle = Puzzle(puzzle.getOriginal(), puzzle.getPuzzle());
        if(solvePuzzleDFS(newPuzzle, i))
            return true;
    }

    return false;
}
