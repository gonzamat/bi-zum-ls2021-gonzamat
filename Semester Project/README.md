# Semester Work

**Introduction to Artificial Intelligence**

**Matias Ignacio Gonzalez**

## Task
**Sudoku at 100%**
Solve the puzzle using artificial intelligence techniques, but so that it is done as quickly and guaranteed as possible, ie the solving algorithm always ends and gives the correct solution.

## Approach chosen

Solve a dataset of 50 sudoku puzzles with three different methods and compare timing.

INPUT: Dataset of puzzles.

OUTPUT: Time and completion percentage for each algorithm.

## Implementation details

Input data from .txt file.
Each row representing one puzzle.

Using <ctime> clock to measure time.

## DFS

Simple DFS algorithm, starting in the top left corner and filling empty cells. Backtracks when there is an inconsistency.

## Smart DFS

Backtracking algorithm using a DFS technique but choosing the cell in a smart way. It looks for the cell that has the least possibilities meaning less branching and as a result, less computation time.

## Steep Hill Climbing

Each new candidate for this algorithm is chosen this way:
1) Take a random row.
2) Shuffle it.

Each new candidate is not the first "better-fit", it generates 200 new candidates and chooses the best of them.

The objective function is the amount of rows, columns and 3x3 squares that are correct. maximum score is 27 which means the puzzle is solved.

## Results

**DFS**:

Solved 50 puzzles out of 50 in 42.483000 seconds


**Smart DFS**:

Solved 50 puzzles out of 50 in 38.736000 seconds


**Hill Climbing**:

Solved 0 puzzles out of 10 in 33.920000 seconds



Smart DFS was faster, but it could improve its speed by refining parameters like how much computation will it invest in finding the best cell and also choosing what number to use.

Hill climbing could not solve any puzzle, given that they were very hard (almost empty puzzles) and the possibilities were too many.
It may be better for solving simpler puzzles.